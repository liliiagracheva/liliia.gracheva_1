require: text/text.sc
    module = zenbot-common

require: number/number.sc
    module = zenbot-common

require: common.js
    module = zenbot-common

require: city/city.sc
    module = zenbot-common

require: city/cities-ru-mainonly.csv
    name = Cities
    var = $Cities
    module = zenbot-common


patterns:
    $hello = (здравствуй*|привет|добрый (день|вечер)| доброе утро|доброй ночи)
    $yes = (ага|ок|окей|да|давай|хорошо|ладно|хочу)
    $no = (нет|не (хочу|буду|сейчас)|неа|стоп|хватит|достаточно|надоело)
    $true = (ага|да|угадал*|правильно|верно|равно)
    $City = $entity<Cities> || converter = $converters.cityConverter
    

theme: /cities

require: text/text.sc
    module = zenbot-common

require: number/number.sc
    module = zenbot-common

require: common.js
    module = zenbot-common

require: city/city.sc
    module = zenbot-common

require: city/cities-ru-mainonly.csv
    name = Cities
    var = $Cities
    module = zenbot-common


patterns:
    $hello = (здравствуй*|привет|добрый (день|вечер)| доброе утро|доброй ночи)
    $yes = (ага|ок|окей|да|давай|хорошо|ладно|хочу)
    $no = (нет|не (хочу|буду|сейчас)|неа|стоп|хватит|достаточно|надоело)
    $true = (ага|да|угадал*|правильно|верно|равно)
    $City = $entity<Cities> || converter = $converters.cityConverter
    

theme: /cities

    state: Hello
      q!: $hello
      # script:
      #   $session.RandomCity
      a: Здравствуйте! Хотите сыграть в игру "Города"?
      
      state: No
        q: $no
        a: Хорошо. Всего доброго!
    
      state: Yes
        q: $yes
        script:
          $session.UsedCities = [];
        a: Пожалуйста, выберите, кто первым загадывает город.
    
          buttons:
            "Программа"
          buttons:  
            "Пользователь"
    
          state: Программа
            script:
              $session.RandomCity = "Санкт-Петербург";
              $session.UsedCities.push($RandomCity);
            a: {{ $session.RandomCity }}
            go!: /User Answer
    
          state: Пользователь  
            q!: $City
            script:
              $session.UsedCities.push($parseTree._City);
            go!: /Bot Answer
    
    
          state: Bot Answer
            script:
              $session.LastWord = $session.UsedCities[$session.UsedCities.length-1];
              $temp.LastLetter = $session.LastWord.slice(-1);
              var tmp_city = ''
              for (let i = 0; i < 8595; i++) { 
                var used = $session.UsedCities.includes(Cities[i].value);
                if ((Cities[i].value.slice(-1) == $temp.LastLetter) && !(used)){
                  tmp_city = Cities[i].value;
                }
              }
              if (tmp_city) {
                $reactions.answer(tmp_city);
              } else {
                $temp.LastLetter = $session.LastWord.slice(-2,-1);
                for (let i = 0; i < 8595; i++) { 
                  var used = $session.UsedCities.includes(Cities[i].value);
                  if ((Cities[i].value.slice(-1) == $temp.LastLetter) && !(used)){
                    tmp_city = Cities[i].value;
                  }
                }
              }
              $reactions.answer(tmp_city);
            go!: /User Answer
    
    
    
          state: User Answer
            q: $City
            script:
              $session.LastWord = $session.UsedCities[$session.UsedCities.length-1]
              $temp.LastLetter = $session.LastWord.slice(-1)
              $temp.FirstLetter = $parseTree._City.slice(0)
              $temp.Used = $session.UsedCities.includes($parseTree._City)
    
            if ( $temp.LastLetter == $temp.FirstLetter ) && !($temp.Used) :
              script:
                $session.UsedCities.push($parseTree._City);
              go!: /Bot Answer
            else:
              a: Ваш ответ некорректен, пожалуйста, введите другой город.
              go!: /User Answer
    
    
                               
    state: CatchAll || noContext = true
      q!: *
      a: Простите, я вас не понимаю.            