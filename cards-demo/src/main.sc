require: address/address.sc
    module = zenbot-common
require: patterns.sc
require: topic/app.sc
require: topic/other.sc
require: topic/profile.sc


patterns:
    $Start = $regexp</?start>

theme: /

#    state: CreateSession
#        q!: $Start
#        newSession:
#            targetState

    state: Welcome
        q!: $Start
        #TODO: ..newSession
        a: Здравствуйте! Я электронный консультант.

    state: CatchAll
        q!: *
        a: Перевожу на оператора.
#        script:
#            responseData.replies = responseData.replies || [];
#            userData.history = parseTree.text;
#            responseData.replies
#             .push({
#                type:"switch",
#                firstMessage: userData.history,
#                lastMessage: 'Клиент закрыл чат',
#            });
 
    state: NoOperatorsOnline
        event: noLivechatOperatorsOnline
        a: К сожалению, сейчас нет операторов онлайн. Оставьте свои контактные данные, и мы с вами свяжемся.

        state: Contacts
            q: *            
#            script:
#                var info = parseTree.text;
#                responseData.replies = responseData.replies || [];
#                responseData.replies
#                 .push({
#                    type:"switch",
#                    firstMessage: userData.history + '\n' + info,
#                    ignoreOffline: true,
#                    oneTimeMessage: true
#                });
#


